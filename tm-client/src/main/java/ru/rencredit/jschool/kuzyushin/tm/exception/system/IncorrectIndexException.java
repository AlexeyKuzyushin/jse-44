package ru.rencredit.jschool.kuzyushin.tm.exception.system;

import ru.rencredit.jschool.kuzyushin.tm.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(String value) {
        super("Error! This value '"+value+"' is not a number...");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }
}
