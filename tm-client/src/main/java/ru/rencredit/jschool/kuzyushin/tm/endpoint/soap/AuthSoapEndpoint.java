package ru.rencredit.jschool.kuzyushin.tm.endpoint.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-03-14T19:28:43.711+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://soap.endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "AuthSoapEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AuthSoapEndpoint {

    @WebMethod
    @RequestWrapper(localName = "logout", targetNamespace = "http://soap.endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.Logout")
    @ResponseWrapper(localName = "logoutResponse", targetNamespace = "http://soap.endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.LogoutResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.Result logout();

    @WebMethod
    @RequestWrapper(localName = "profile", targetNamespace = "http://soap.endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.Profile")
    @ResponseWrapper(localName = "profileResponse", targetNamespace = "http://soap.endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.ProfileResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.User profile();

    @WebMethod
    @RequestWrapper(localName = "login", targetNamespace = "http://soap.endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.Login")
    @ResponseWrapper(localName = "loginResponse", targetNamespace = "http://soap.endpoint.tm.kuzyushin.jschool.rencredit.ru/", className = "ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.LoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.Result login(
        @WebParam(name = "username", targetNamespace = "")
        java.lang.String username,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );
}
