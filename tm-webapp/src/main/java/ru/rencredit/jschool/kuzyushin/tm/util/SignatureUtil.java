package ru.rencredit.jschool.kuzyushin.tm.util;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface SignatureUtil {

    static String sign(final Object value, final String salt, final Integer cycle) {
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        }
        catch (Exception e) {
            return null;
        }
    }

    public static String sign(String value, String salt, Integer cycle) {
        if (value == null || salt == null) return null;
        String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.md5(salt + result + salt);
        }
        return result;
    }
}
