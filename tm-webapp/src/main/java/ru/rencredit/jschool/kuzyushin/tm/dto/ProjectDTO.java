package ru.rencredit.jschool.kuzyushin.tm.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.rencredit.jschool.kuzyushin.tm.constant.DateConstant;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDTO extends AbstractEntityDTO implements Serializable {

    public static final long serialVersionUID = 1;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @Nullable
    private String userId;

    @Nullable
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern= DateConstant.DATE_FORMAT)
    private Date startDate;

    @Nullable
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern= DateConstant.DATE_FORMAT)
    private Date finishDate;

    @Nullable
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern= DateConstant.DATE_FORMAT)
    private Date creationDate;

    @Override
    public String toString() { return getName() + ": " + name;}

    @Nullable
    public static ProjectDTO toDTO(final @Nullable Project project) {
        if (project == null) return null;
        return new ProjectDTO(project);
    }

    @NotNull
    public static List<ProjectDTO> toDTO(final @Nullable List<Project> projects) {
        if (projects == null || projects.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectDTO> result = new ArrayList<>();
        for (@Nullable final Project project: projects) {
            if (project == null) continue;
            result.add(new ProjectDTO(project));
        }
        return result;
    }

    public ProjectDTO(final @Nullable Project project) {
        if (project == null) return;
        id = project.getId();
        name = project.getName();
        description = project.getDescription();
        userId = project.getUser().getId();
        if (project.getCreationTime() != null) creationDate = project.getCreationTime();
        if (project.getStartDate() != null) startDate = project.getStartDate();
        if (project.getFinishDate() != null) finishDate = project.getFinishDate();
    }
}
