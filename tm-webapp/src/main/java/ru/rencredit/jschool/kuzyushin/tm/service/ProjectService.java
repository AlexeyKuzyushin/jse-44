package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectStartDateException;

import java.util.Date;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectRepository projectRepository;

    @Autowired
    public ProjectService(
            final @NotNull IUserService userService,
            final @NotNull IProjectRepository projectRepository
    ) {
        this.userService = userService;
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public Long count() {
        @NotNull final Long countOfProjects = projectRepository.count();
        return countOfProjects;
    }

    @Override
    @Nullable
    public  Project getProjectById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.getOne(id);
        return project;
    }

    @Override
    @Transactional
    public void removeProjectById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteById(id);
    }

    @Override
    @Transactional
    public @Nullable Project findProjectById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findById(id).orElse(null);
        return project;
    }

    @Override
    @Transactional
    public void updateProjectById(final @Nullable String id,
                                  final @Nullable String name,
                                  final @Nullable String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Project project = findProjectById(id);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return ProjectDTO.toDTO(projectRepository.findAll());
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return ProjectDTO.toDTO(projectRepository.findAllByUserId(userId));
    }

    @Override
    @Transactional
    public Project create(final @Nullable String userId,
                       final @Nullable String name,
                       final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(userService.findById(userId));
        return projectRepository.save(project);
    }

    @Nullable
    @Override
    public Project findById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findByUserIdAndId(userId, id);
        return project;
    }

    @Nullable
    @Override
    public Project findByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectRepository.findByUserIdAndName(userId, name);
        return project;
    }

    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectRepository.deleteByUserIdAndName(userId, name);
    }

    @Nullable
    @Override
    @Transactional
    public Project updateById(
            final @Nullable String userId, final @Nullable String id,
            final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Project project = findById(userId, id);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
        return project;
    }

    @Override
    @Transactional
    public void updateStartDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Project project = findById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        project.setStartDate(date);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void updateFinishDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId == null) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Project project = findById(userId, id);
        assert project != null;
        if (project.getStartDate() == null) throw new IncorrectStartDateException();
        if (project.getStartDate().after(date)) throw new IncorrectStartDateException(date);
        project.setFinishDate(date);
        projectRepository.save(project);
    }

    @Override
    public void load(final @Nullable List<ProjectDTO> projects) {
        if (projects == null) return;
        projectRepository.deleteAll();
        for (final ProjectDTO projectDTO: projects){
            @NotNull final Project project = new Project();
            project.setName(projectDTO.getName());
            project.setDescription(projectDTO.getDescription());
            project.setUser(userService.findById(projectDTO.getUserId()));
            project.setId(projectDTO.getId());
            project.setStartDate(projectDTO.getStartDate());
            project.setFinishDate(projectDTO.getFinishDate());
            project.setCreationTime(projectDTO.getCreationDate());
            projectRepository.save(project);
        }
    }
}
