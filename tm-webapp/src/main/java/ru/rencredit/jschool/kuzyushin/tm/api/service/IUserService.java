package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.util.List;

public interface IUserService {

    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User getOneById(@Nullable String id);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    void updateLogin(@Nullable String id, @Nullable String login);

    void updatePassword(@Nullable String id, @Nullable String password);

    void updateEmail(@Nullable String id, @Nullable String email);

    void updateFirstName(@Nullable String id, @Nullable String firstName);

    void updateLastName(@Nullable String id, @Nullable String lastName);

    void updateMiddleName(@Nullable String id, @Nullable String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

    void load(@Nullable List<UserDTO> tasks);
}
