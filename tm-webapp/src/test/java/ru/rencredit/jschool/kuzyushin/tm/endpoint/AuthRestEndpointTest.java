package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.rencredit.jschool.kuzyushin.tm.config.WebMvcConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.repository.IUserRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class AuthRestEndpointTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    private MockMvc mockMvc;

    private static final String adminCredentials = "adminTest";

    private static final String baseUrl = "/rest/auth";

    private static final User admin = new User();

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        admin.setLogin(adminCredentials);
        admin.setPasswordHash(passwordEncoder.encode(adminCredentials));
        userRepository.save(admin);
    }

    @After
    public void delete() {
        userRepository.deleteAll();
    }

    @Test
    public void testLogin() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/login")
                .param("username", adminCredentials)
                .param("password", adminCredentials))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void testLogout() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/login")
                .param("username", adminCredentials)
                .param("password", adminCredentials));
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/logout"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

    @Test
    public void testProfile() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/login")
                .param("username", adminCredentials)
                .param("password", adminCredentials));
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/profile")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.login").value(adminCredentials));
    }

    @Test
    public void unauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/rest/project"))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }
}
