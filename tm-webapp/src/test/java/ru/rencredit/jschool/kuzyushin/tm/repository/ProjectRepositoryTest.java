package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.config.WebMvcConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.transaction.Transactional;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class ProjectRepositoryTest {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private IUserRepository userRepository;

    private static final User admin = new User();

    private static final User test = new User();

    private static final Project projectOne = new Project();

    private static final Project projectTwo = new Project();

    @Autowired
    private static PasswordEncoder passwordEncoder;

    private static final String adminCredentials = "adminTest";

    private static final String testCredentials = "userTest";

    @BeforeClass
    public static void set() {
        admin.setLogin(adminCredentials);
        admin.setEmail("admin@admin.com");
        admin.setPasswordHash("password-admin");
        admin.setFirstName("first-name-admin");
        admin.setLastName("last-name-admin");
        admin.setMiddleName("middle-name-admin");
        admin.setRole(Role.ADMIN);

        test.setLogin(testCredentials);
        test.setEmail("test@test.com");
        test.setPasswordHash("password-test");
        test.setFirstName("first-name-test");
        test.setLastName("last-name-test");
        test.setMiddleName("middle-name-test");
        test.setRole(Role.USER);

        projectOne.setName("projectOne");
        projectOne.setDescription("projectOne");
        projectOne.setUser(admin);
        projectTwo.setName("projectTwo");
        projectTwo.setDescription("projectTwo");
        projectTwo.setUser(test);
    }

    @Before
    @Transactional
    public void init() {
        userRepository.save(admin);
        userRepository.save(test);

        projectRepository.save(projectOne);
        projectRepository.save(projectTwo);
    }

    @After
    @Transactional
    public void delete() {
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void count() {
        Assert.assertEquals(2, projectRepository.count());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        Project project = new Project();
        project.setUser(test);
        projectRepository.save(project);
        Assert.assertEquals(2, projectRepository.findAllByUserId(test.getId()).size());
    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final Project project = projectRepository.findByUserIdAndId(admin.getId(), projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectOne.getId());
    }

    @Test
    public void findByUserIdAndNameTest() {
        @Nullable final Project project = projectRepository.findByUserIdAndName(admin.getId(), projectOne.getName());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectOne.getId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
    }

    @Test
    @Transactional
    public void deleteTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.delete(projectOne);
        Assert.assertEquals(1, projectRepository.findAll().size());
        @Nullable final Project project = projectRepository.findById(projectOne.getId()).orElse(null);
        Assert.assertNull(project);
    }

    @Test
    @Transactional
    public void deleteByIdTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.deleteById(projectOne.getId());
        Assert.assertEquals(1, projectRepository.findAll().size());
        @Nullable final Project project = projectRepository.findById(projectOne.getId()).orElse(null);
        Assert.assertNull(project);
    }

    @Test
    @Transactional
    public void deleteByUserIdAndIdTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.deleteByUserIdAndId(admin.getId(), projectOne.getId());
        Assert.assertEquals(1, projectRepository.findAll().size());
        Assert.assertNull(projectRepository.findById(projectOne.getId()).orElse(null));
    }

    @Test
    @Transactional
    public void deleteByUserIdAndNameTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        projectRepository.deleteByUserIdAndName(admin.getId(), projectOne.getName());
        Assert.assertEquals(1, projectRepository.findAll().size());
        Assert.assertNull(projectRepository.findById(projectOne.getId()).orElse(null));
    }

    @Test
    @Transactional
    public void deleteAllByUserIdTest() {
        Assert.assertEquals(2, projectRepository.findAll().size());
        Project project = new Project();
        project.setUser(admin);
        projectRepository.save(project);
        Assert.assertEquals(3, projectRepository.findAll().size());
        projectRepository.deleteAllByUserId(admin.getId());
        Assert.assertEquals(1, projectRepository.findAll().size());
    }
}
