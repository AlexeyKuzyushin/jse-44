package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.rencredit.jschool.kuzyushin.tm.config.WebMvcConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.IUserRepository;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class ProjectRestEndpointTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private MockMvc mockMvc;

    private static final String adminCredentials = "adminTest";

    private final String baseUrl = "/rest/projects";

    private static final User admin = new User();

    private static final Project projectOne = new Project();

    @BeforeClass
    public static void set() {
        projectOne.setName("projectOne");
        projectOne.setDescription("projectOne");
        projectOne.setUser(admin);
    }

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        admin.setLogin(adminCredentials);
        admin.setPasswordHash(passwordEncoder.encode(adminCredentials));

        userRepository.save(admin);
        projectRepository.save(projectOne);

        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(adminCredentials, adminCredentials);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void delete() {
        userRepository.deleteAll();
        projectRepository.deleteAll();
    }

    @Test
    public void createTest() throws Exception {
        @NotNull final String name = "testName";
        @NotNull final String description = "testDescription";
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter objectWriter = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = objectWriter.writeValueAsString(projectDTO);

        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.description").value(description));
    }

    @Test
    public void updateByIdTest() throws Exception {
        @NotNull final String name = "other-name";
        @NotNull final String description = "other-description";
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(projectOne.getId());
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(projectDTO);

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(baseUrl + "/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value(projectOne.getId()))
                .andExpect(jsonPath("$.name").value(name))
                .andExpect(jsonPath("$.description").value(description));
    }

    @Test
    public void countAllProjectsTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/count"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("1"));
    }

    @Test
    public void findByIdTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/find/{id}", projectOne.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value(projectOne.getId()));
    }

    @Test
    public void findAllTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/findAll"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..name", hasItem(projectOne.getName())));
    }

    @Test
    public void removeByIdTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(baseUrl + "/remove/{id}", projectOne.getId()))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
        @Nullable final Project project = projectRepository.findById(projectOne.getId()).orElse(null);
        Assert.assertNull(project);
    }
}
