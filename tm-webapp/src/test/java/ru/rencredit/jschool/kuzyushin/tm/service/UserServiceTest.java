package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.config.WebMvcConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.repository.IUserRepository;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class UserServiceTest {

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final String adminCredentials = "adminTest";

    private static final String testCredentials = "userTest";

    private static User admin;

    private static User test;

    @Before()
    public void init() {
        admin = userService.create(adminCredentials,adminCredentials,Role.ADMIN);
        test = userService.create(testCredentials,testCredentials,Role.USER);
    }

    @After
    public void deleteUser() {
        userRepository.deleteAll();
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final User user = userService.findById(admin.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), admin.getId());
    }

    @Test
    public void findByLoginTest() {
        @Nullable final User user = userService.findByLogin(admin.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), admin.getId());
    }

    @Test
    public void removeByIdTest() {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeById(admin.getId());
        Assert.assertEquals(1, userService.findAll().size());
        Assert.assertNull(userService.findById(admin.getId()));
    }

    @Test
    public void removeByLoginTest() {
        Assert.assertEquals(2, userService.findAll().size());
        userService.removeByLogin(test.getLogin());
        Assert.assertEquals(1, userService.findAll().size());
        Assert.assertNull(userService.findById(test.getId()));
    }

    @Test
    public void createUserWithEmailTest() {
        @NotNull final String login = "testUser";
        @NotNull final String password = "987987";
        @NotNull final String email = "testUser@email.com";
        userService.create(login, password, email);
        @Nullable final User user = userService.findByLogin(login);
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(email, user.getEmail());
    }

    @Test
    public void createUserWithRoleTest() {
        @NotNull final String login = "adminUser";
        @NotNull final String password = "765765";
        userService.create(login, password, Role.ADMIN);
        @Nullable final User user = userService.findByLogin(login);
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void updateLoginTest() {
        @Nullable final User user = userService.findById(test.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldLogin = user.getLogin();
        userService.updateLogin(test.getId(), "other-user");
        @Nullable final User otherUser = userService.findById(test.getId());
        Assert.assertNotNull(otherUser);
        @Nullable final String newLogin = otherUser.getLogin();
        Assert.assertNotEquals(oldLogin, newLogin);
    }

    @Test
    public void updatePasswordTest() {
        @Nullable final User user = userService.findById(admin.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldPassword = user.getPasswordHash();
        userService.updatePassword(admin.getId(), "other-password");
        @Nullable final User otherUser = userService.findById(admin.getId());
        Assert.assertNotNull(otherUser);
        @Nullable final String newPassword = otherUser.getPasswordHash();
        Assert.assertNotEquals(oldPassword, newPassword);
    }

    @Test
    public void updateUserFirstNameTest() {
        @Nullable final User user = userService.findById(admin.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldFirstName = user.getFirstName();
        userService.updateFirstName(admin.getId(), "other-first-name");
        @Nullable final User otherUser = userService.findById(admin.getId());
        Assert.assertNotNull(otherUser);
        @Nullable final String newFirstName = otherUser.getFirstName();
        Assert.assertNotEquals(oldFirstName, newFirstName);
    }

    @Test
    public void updateUserMiddleNameTest() {
        @Nullable final User user = userService.findById(admin.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldMiddleName = user.getMiddleName();
        userService.updateMiddleName(admin.getId(), "other-middle-name");
        @Nullable final User otherUser = userService.findById(admin.getId());
        Assert.assertNotNull(otherUser);
        @Nullable final String newMiddleName = otherUser.getMiddleName();
        Assert.assertNotEquals(oldMiddleName, newMiddleName);
    }

    @Test
    public void updateUserLastNameTest() {
        @Nullable final User user = userService.findById(admin.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldLastName = user.getLastName();
        userService.updateLastName(admin.getId(), "other-last-name");
        @Nullable final User otherUser = userService.findById(admin.getId());
        Assert.assertNotNull(otherUser);
        @Nullable final String newLastName = otherUser.getLastName();
        Assert.assertNotEquals(oldLastName, newLastName);
    }

    @Test
    public void updateUserEmailTest() {
        @Nullable final User user = userService.findById(admin.getId());
        Assert.assertNotNull(user);
        @Nullable final String oldEmail = user.getEmail();
        userService.updateEmail(admin.getId(), "other-email");
        @Nullable final User otherUser = userService.findById(admin.getId());
        Assert.assertNotNull(otherUser);
        @Nullable final String newEmail = otherUser.getEmail();
        Assert.assertNotEquals(oldEmail, newEmail);
    }

    @Test
    public void lockUserByLoginTest() {
        Assert.assertFalse(userService.findById(admin.getId()).getLocked());
        userService.lockUserByLogin(admin.getLogin());
        Assert.assertTrue(userService.findById(admin.getId()).getLocked());
        userService.unlockUserByLogin(admin.getLogin());
        Assert.assertFalse(userService.findById(admin.getId()).getLocked());
    }
}
