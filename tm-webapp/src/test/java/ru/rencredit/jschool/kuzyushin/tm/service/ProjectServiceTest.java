package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.config.WebMvcConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.dto.CustomUser;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.IUserRepository;

import java.util.ArrayList;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class ProjectServiceTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final String adminCredentials = "adminTest";

    private static final String testCredentials = "userTest";

    private CustomUser admin;

    private CustomUser test;

    private Project projectOne;

    private Project projectTwo;

    @Before()
    public void init() {
        admin = new CustomUser(adminCredentials, adminCredentials, new ArrayList<>(),
                userService.create(adminCredentials, adminCredentials, Role.ADMIN).getId());
        test = new CustomUser(testCredentials, testCredentials, new ArrayList<>(),
                userService.create(testCredentials, testCredentials, Role.USER).getId());

        projectOne = projectService.create(admin.getUserId(), "projectOne", "projectOne");
        projectTwo = projectService.create(test.getUserId(), "projectTwo", "projectTwo");

        projectRepository.save(projectOne);
        projectRepository.save(projectTwo);
    }

    @After
    public void clearData() {
        userRepository.deleteAll();
        projectService.clear();
    }

    @Test
    public void countAllProjectsTest() {
        Assert.assertEquals(2, projectService.count().intValue());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectService.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(1, projectService.findAllByUserId(admin.getUserId()).size());
        Assert.assertEquals(1, projectService.findAllByUserId(test.getUserId()).size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final Project project = projectService.findProjectById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectOne.getId());
    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final Project project = projectService.findById(test.getUserId(), projectTwo.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectTwo.getId());
    }

    @Test
    public void findByNameTest() {
        @Nullable final Project project = projectService.findByName(admin.getUserId(), projectOne.getName());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectOne.getId());
    }

    @Test
    public void removeProjectByIdTest() {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeProjectById(projectOne.getId());
        Assert.assertEquals(1, projectService.findAll().size());
        Assert.assertNull(projectService.findProjectById(projectOne.getId()));
    }

    @Test
    public void removeByUserIdAndIdTest() {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeById(admin.getUserId(), projectOne.getId());
        Assert.assertEquals(1, projectService.findAll().size());
        Assert.assertNull(projectService.findProjectById(projectOne.getId()));
    }

    @Test
    public void removeByNameTest() {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeByName(admin.getUserId(), projectOne.getName());
        Assert.assertEquals(1, projectService.findAll().size());
        Assert.assertNull(projectService.findProjectById(projectOne.getId()));
    }

    @Test
    public void removeAllTest() {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.clear();
        Assert.assertEquals(0, projectService.findAll().size());
    }

    @Test
    public void removeAllByUserIdTest() {
        Assert.assertEquals(2, projectService.findAll().size());
        projectService.removeAllByUserId(admin.getUserId());
        Assert.assertEquals(1, projectService.findAll().size());
    }

    @Test
    public void updateByIdTest() {
        @NotNull final Project otherProject = new Project();
        @NotNull final String name = "other-name";
        @NotNull final String description = "other-description";
        otherProject.setId(projectOne.getId());
        otherProject.setName(name);
        otherProject.setDescription(description);
        projectService.updateById(admin.getUserId(), otherProject.getId(), otherProject.getName(),
                otherProject.getDescription());
        @Nullable final Project project = projectService.findProjectById(projectOne.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getId(), projectOne.getId());
        Assert.assertEquals(project.getName(), name);
        Assert.assertEquals(project.getDescription(), description);
    }
}
