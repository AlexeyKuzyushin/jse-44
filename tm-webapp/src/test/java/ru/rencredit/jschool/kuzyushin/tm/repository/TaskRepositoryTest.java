package ru.rencredit.jschool.kuzyushin.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.config.WebMvcConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.transaction.Transactional;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class TaskRepositoryTest {

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private IUserRepository userRepository;

    private static final User admin = new User();

    private static final User test = new User();

    private static final Project projectOne = new Project();

    private static final Project projectTwo = new Project();

    private static final Task taskOne = new Task();

    private static final Task taskTwo = new Task();

    @Autowired
    private static PasswordEncoder passwordEncoder;

    private static final String adminCredentials = "adminTest";

    private static final String testCredentials = "userTest";

    @BeforeClass
    public static void set() {
        admin.setLogin(adminCredentials);
        admin.setEmail("admin@admin.com");
        admin.setPasswordHash("password-admin");
        admin.setFirstName("first-name-admin");
        admin.setLastName("last-name-admin");
        admin.setMiddleName("middle-name-admin");
        admin.setRole(Role.ADMIN);

        test.setLogin(testCredentials);
        test.setEmail("test@test.com");
        test.setPasswordHash("password-test");
        test.setFirstName("first-name-test");
        test.setLastName("last-name-test");
        test.setMiddleName("middle-name-test");
        test.setRole(Role.USER);

        projectOne.setName("projectOne");
        projectOne.setDescription("projectOne");
        projectOne.setUser(admin);
        projectTwo.setName("projectTwo");
        projectTwo.setDescription("projectTwo");
        projectTwo.setUser(test);

        taskOne.setName("taskOne");
        taskOne.setDescription("taskOne");
        taskOne.setProject(projectOne);
        taskOne.setUser(admin);
        taskTwo.setName("taskTwo");
        taskTwo.setDescription("taskTwo");
        taskTwo.setProject(projectTwo);
        taskTwo.setUser(test);
    }

    @Before
    @Transactional
    public void init() {
        userRepository.save(admin);
        userRepository.save(test);

        projectRepository.save(projectOne);
        projectRepository.save(projectTwo);

        taskRepository.save(taskOne);
        taskRepository.save(taskTwo);
    }

    @After
    @Transactional
    public void delete() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void count() {
        Assert.assertEquals(2, taskRepository.count());
    }

    @Test
    public void findByUserIdAndIdTest() {
        @Nullable final Task task = taskRepository.findByUserIdAndId(test.getId(), taskTwo.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), taskTwo.getId());
    }

    @Test
    public void findByUserIdAndNameTest() {
        @Nullable final Task task = taskRepository.findByUserIdAndName(admin.getId(), taskOne.getName());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), taskOne.getId());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        Task task = new Task();
        task.setUser(admin);
        taskRepository.save(task);
        Assert.assertEquals(2, taskRepository.findAllByUserId(admin.getId()).size());
        Assert.assertEquals(1, taskRepository.findAllByUserId(test.getId()).size());
    }

    @Test
    public void findAllByProjectIdTest() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        Task task = new Task();
        task.setProject(projectOne);
        taskRepository.save(task);
        Assert.assertEquals(2, taskRepository.findAllByProjectId(projectOne.getId()).size());
        Assert.assertEquals(1, taskRepository.findAllByProjectId(projectTwo.getId()).size());
    }

    @Test
    @Transactional
    public void deleteByUserIdAndIdTest() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        taskRepository.deleteByUserIdAndId(admin.getId(), taskOne.getId());
        Assert.assertEquals(1, taskRepository.findAll().size());
        Assert.assertNull(taskRepository.findById(taskOne.getId()).orElse(null));
    }

    @Test
    @Transactional
    public void deleteByUserIdAndNameTest() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        taskRepository.deleteByUserIdAndName(test.getId(), taskTwo.getName());
        Assert.assertEquals(1, taskRepository.findAll().size());
        Assert.assertNull(taskRepository.findById(taskTwo.getId()).orElse(null));
    }

    @Test
    @Transactional
    public void deleteAllByUserIdTest() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        Task task = new Task();
        task.setUser(admin);
        taskRepository.save(task);
        Assert.assertEquals(3, taskRepository.findAll().size());
        taskRepository.deleteAllByUserId(admin.getId());
        Assert.assertEquals(1, taskRepository.findAll().size());
    }

    @Test
    @Transactional
    public void deleteAllByProjectIdTest() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        Task task = new Task();
        task.setProject(projectOne);
        taskRepository.save(task);
        Assert.assertEquals(3, taskRepository.findAll().size());
        taskRepository.deleteAllByProjectId(projectOne.getId());
        Assert.assertEquals(1, taskRepository.findAll().size());
    }
}
