package ru.rencredit.jschool.kuzyushin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.rencredit.jschool.kuzyushin.tm.config.WebMvcConfiguration;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.repository.IUserRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class ProjectControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private MockMvc mockMvc;

    private static final String baseUrl = "/projects";

    private static final String adminCredentials = "adminTest";

    private static final User admin = new User();

    private static final Project projectOne = new Project();

    private static final Project projectTwo = new Project();

    @BeforeClass
    public static void set() {
        projectOne.setName("projectOne");
        projectOne.setDescription("projectOne");
        projectOne.setUser(admin);

        projectTwo.setName("projectTwo");
        projectTwo.setDescription("projectTwo");
        projectTwo.setUser(admin);
    }

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        admin.setLogin(adminCredentials);
        admin.setPasswordHash(passwordEncoder.encode(adminCredentials));
        userRepository.save(admin);
        projectRepository.save(projectOne);
        projectRepository.save(projectTwo);

        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(adminCredentials, adminCredentials);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void delete() {
        userRepository.deleteAll();
        projectRepository.deleteAll();
    }

    @Test
    public void showTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/project/project-list"));
    }

    @Test
    public void createGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/create"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/project/project-create"));
    }

    @Test
    public void createPostTest() throws Exception {
        @NotNull final Project project = new Project();
        project.setName("test");
        project.setDescription("test");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/create")
                .flashAttr("project", project))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/projects"))
                .andExpect(view().name("redirect:/projects"));
    }

    @Test
    public void deleteTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/delete/{id}", projectOne.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/projects"))
                .andExpect(view().name("redirect:/projects"));
    }

    @Test
    public void updateGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/update/{id}", projectOne.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/project/project-update"));
    }

    @Test
    public void updatePostTest() throws Exception {
        @NotNull final Project project = new Project();
        project.setName("other-name");
        project.setDescription("other-description");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/update/{id}", projectOne.getId())
                .flashAttr("project", project))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(redirectedUrl("/projects"))
                .andExpect(view().name("redirect:/projects"));
    }

    @Test
    public void viewTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/view/{id}", projectOne.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/project/project-view"));
    }
}
